package com.cartech.mainViewTabControllers;


import com.cartech.models.*;
import com.cartech.repository.*;
import com.cartech.viewControllers.MainViewController;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class UserTabController {
	
	private MainViewController mainViewController;
	private UserRepository userRepository;
	private CarRepository carRepository;
	

	public UserTabController(MainViewController mainViewController){
		this.mainViewController = mainViewController;		
	}
	
	public void runOnMainViewFXMLInit(){
		mainViewController.userFirstNameColumn.setCellValueFactory(cellData -> cellData.getValue().getFirstNameProperty());
		mainViewController.userLastNameColumn.setCellValueFactory(cellData -> cellData.getValue().getLastNameProperty());
		mainViewController.userPhoneNumberColumn.setCellValueFactory(cellData -> cellData.getValue().getPhoneNumberProperty());
		
		mainViewController.userCarBrandColumn.setCellValueFactory(cellData -> cellData.getValue().getBrandProperty());
		mainViewController.userCarModelColumn.setCellValueFactory(cellData -> cellData.getValue().getModelProperty());
		mainViewController.userCarLicencePlateColumn.setCellValueFactory(cellData -> cellData.getValue().getLicencePlateProperty());
		
		showUserDetailsDialog(null);
		
		mainViewController.userTable.getSelectionModel().selectedItemProperty().addListener(
	            (observable, oldValue, newValue) -> showUserDetailsDialog(newValue));
		
		mainViewController.userCarsTable.getSelectionModel().selectedItemProperty().addListener(
	            (observable, oldValue, newValue) -> setSelectedCarFromUserCarsTableInCarTab(newValue));		
	}
	
	private void showUserDetailsDialog(UserModel userModel){
		if(userModel != null){
			mainViewController.userFirstNameLabel.setText(userModel.getFirstName());
			mainViewController.userLastNameLabel.setText(userModel.getLastName());
			mainViewController.userSocialSecurityLabel.setText(userModel.getSocialSecurityNumber());
			mainViewController.userStreetAdressLabel.setText(userModel.getStreetAdress());
			mainViewController.userZipCodeLabel.setText(userModel.getZipCode());
			mainViewController.userCityLabel.setText(userModel.getCity());
			mainViewController.userPhoneNumberLabel.setText(userModel.getPhoneNumber());
			mainViewController.userEmailLabel.setText(userModel.getEmail());
			mainViewController.userUserNameLabel.setText(userModel.getUsername());
			mainViewController.userIdLabel.setText(userModel.getID().toString());
			if (userModel.isEmployee()) {
				mainViewController.userEmployeeLabel.setText("Anst�lld");
			}else{
				mainViewController.userEmployeeLabel.setText("Ej Anst�lld");
			}
			carRepository = new CarRepository();
			mainViewController.userCarsTable.setItems(carRepository.getCarListFromDataBaseForTableViewWithUserID(userModel.getID()));						
		}else{
			mainViewController.userFirstNameLabel.setText("");
			mainViewController.userLastNameLabel.setText("");
			mainViewController.userSocialSecurityLabel.setText("");
			mainViewController.userStreetAdressLabel.setText("");
			mainViewController.userZipCodeLabel.setText("");
			mainViewController.userCityLabel.setText("");
			mainViewController.userPhoneNumberLabel.setText("");
			mainViewController.userEmailLabel.setText("");
			mainViewController.userUserNameLabel.setText("");
			mainViewController.userEmployeeLabel.setText("");
			mainViewController.userIdLabel.setText("");
		}				
	}
	
	private void setSelectedCarFromUserCarsTableInCarTab(CarModel carModel){
		if (carModel.getID() != null) {
			try {
				mainViewController.carSearchTextField.setText(carModel.toString());
				mainViewController.handleCarSearchTextField();
				mainViewController.carTable.getSelectionModel().selectFirst();
				mainViewController.mainViewTabPane.getSelectionModel().select(mainViewController.carTab);
			} catch (Exception exception) {
				System.out.println("Exception in UserTabController.setSelectedCarFromUserCarsTableInCarTab: " + exception.getMessage());
			}
		}
	}
	
	public void handleAddUserButton(){
		UserModel newUser = new UserModel();
		boolean okClicked = mainViewController.mainApplication.showUserEditOrAddDialog(newUser);
		if (okClicked) {
			userRepository = new UserRepository();
			userRepository.addUserToDatabase(newUser);
			mainViewController.updateAllTableViews();
		}
	}
	
	public void handleEditUserButton(){
		UserModel selectedUser = mainViewController.userTable.getSelectionModel().getSelectedItem();
		if (selectedUser != null) {
			boolean okClicked = mainViewController.mainApplication.showUserEditOrAddDialog(selectedUser);
			if (okClicked) {
				userRepository = new UserRepository();
				userRepository.updateUserInDatabase(selectedUser);
				mainViewController.updateAllTableViews();
			}
		}else{
			Alert alert = new Alert(AlertType.WARNING);
	        alert.initOwner(mainViewController.mainApplication.getPrimaryStage());
	        alert.setTitle("Ingen anv�ndare vald");
	        alert.setHeaderText(null);
	        alert.setContentText("Du m�ste v�lja en anv�ndare att �ndra");

	        alert.showAndWait();
		}
	}
	
	public void handleDeleteUserButton(){
		UserModel selectedUser = mainViewController.userTable.getSelectionModel().getSelectedItem();
		if (selectedUser != null) {
			userRepository = new UserRepository();
			userRepository.deleteUserInDatabase(selectedUser);
			mainViewController.updateAllTableViews();
		}else{
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Ingen anv�ndare vald");
			alert.setHeaderText(null);
			alert.setContentText("Du m�ste v�lja en anv�ndare att ta bort");
			
			alert.showAndWait();
		}    
	}
	
	public void handleUserSearchTextField(){
		userRepository = new UserRepository();
		mainViewController.userTable.setItems(userRepository.getUserListFromDatabaseForTableView(mainViewController.userSearchTextField.getText()));
	}
}
