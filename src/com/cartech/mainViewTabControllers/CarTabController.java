package com.cartech.mainViewTabControllers;


import com.cartech.models.*;
import com.cartech.repository.*;
import com.cartech.viewControllers.MainViewController;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class CarTabController {
	
	private MainViewController mainViewController;
	private UserRepository userRepository;
	private CarRepository carRepository;
	private ServiceRepository serviceRepository;
	

	public CarTabController(MainViewController mainViewController){
		this.mainViewController = mainViewController;
	}
	
	public void runOnMainViewFXMLInit(){
		mainViewController.carBrandColumn.setCellValueFactory(cellData -> cellData.getValue().getBrandProperty());
		mainViewController.carModelColumn.setCellValueFactory(cellData -> cellData.getValue().getModelProperty());
		mainViewController.carLicencePlateColumn.setCellValueFactory(cellData -> cellData.getValue().getLicencePlateProperty());
		
		mainViewController.carServiceDateColumn.setCellValueFactory(cellData -> cellData.getValue().getServiceDateProperty());
		mainViewController.carServiceTimeColumn.setCellValueFactory(cellData -> cellData.getValue().getServiceTimeProperty());
		
		showCarDetailsDialog(null);
		
		mainViewController.carTable.getSelectionModel().selectedItemProperty().addListener(
	            (observable, oldValue, newValue) -> showCarDetailsDialog(newValue));
		
		mainViewController.carServicesTable.getSelectionModel().selectedItemProperty().addListener(
	            (observable, oldValue, newValue) -> setSelectedServiceFromCarServicesTableInServiceTab(newValue));
		
	}
	
	private void showCarDetailsDialog(CarModel carModel){
		if(carModel != null){
			userRepository = new UserRepository();
			UserModel userModel = new UserModel();
			userModel = userRepository.getUserFromDataBaseWithUserID(carModel.getOwnerID());	
			
			mainViewController.carIDLabel.setText(carModel.getID().toString());
			mainViewController.carOwnerFirstAndLastNameLabel.setText(userModel.getFirstName() + " " + userModel.getLastName());
			mainViewController.carOwnerPhoneLabel.setText(userModel.getPhoneNumber());
			mainViewController.carBrandLabel.setText(carModel.getBrand());
			mainViewController.carModelLabel.setText(carModel.getModel());
			mainViewController.carLicencePlateLabel.setText(carModel.getLicencePlate());
			mainViewController.carCommentsTextArea.setText(carModel.getComments());
			
			serviceRepository = new ServiceRepository();
			mainViewController.carServicesTable.setItems(serviceRepository.getServiceListFromDataBaseForTableViewWithCarID(carModel.getID()));			
		}else{
			mainViewController.carIDLabel.setText("");
			mainViewController.carOwnerFirstAndLastNameLabel.setText("");
			mainViewController.carOwnerPhoneLabel.setText("");
			mainViewController.carBrandLabel.setText("");
			mainViewController.carModelLabel.setText("");
			mainViewController.carLicencePlateLabel.setText("");
			mainViewController.carCommentsTextArea.setText("");
		}				
	}
	
	private void setSelectedServiceFromCarServicesTableInServiceTab(ServiceModel serviceModel){
		if (serviceModel.getID() != null) {
			try {
				mainViewController.serviceSearchTextField.setText(serviceModel.toString());
				mainViewController.handleServiceSearchTextField();
				mainViewController.serviceTable.getSelectionModel().selectFirst();
				mainViewController.mainViewTabPane.getSelectionModel().select(mainViewController.serviceTab);
			} catch (Exception exception) {
				System.out.println("Exception in CarTabController.setSelectedServiceFromCarServicesTableInServiceTab: " + exception.getMessage());
			}
		}
	}	
	
	public void handleAddCarButton(){
		CarModel newCar = new CarModel();
		boolean okClicked = mainViewController.mainApplication.showCarEditOrAddDialog(newCar);
		if (okClicked) {
			carRepository = new CarRepository();
			carRepository.addCarToDatabase(newCar);
			mainViewController.updateAllTableViews();
			
		}
	}
	
	public void handleEditCarButton(){
		CarModel selectedCar = mainViewController.carTable.getSelectionModel().getSelectedItem();
		if (selectedCar != null) {
			boolean okClicked = mainViewController.mainApplication.showCarEditOrAddDialog(selectedCar);
			if (okClicked) {
				carRepository = new CarRepository();
				carRepository.updateCarInDatabase(selectedCar);
				mainViewController.updateAllTableViews();
			}
		}else{
			Alert alert = new Alert(AlertType.WARNING);
	        alert.initOwner(mainViewController.mainApplication.getPrimaryStage());
	        alert.setTitle("Ingen bil vald");
	        alert.setHeaderText(null);
	        alert.setContentText("Du m�ste v�lja en bil att �ndra");

	        alert.showAndWait();
		}
	}
	
	public void handleDeleteCarButton(){
		CarModel selectedCar = mainViewController.carTable.getSelectionModel().getSelectedItem();
		if (selectedCar != null) {
			carRepository = new CarRepository();
			carRepository.deleteCarInDatabase(selectedCar);
			mainViewController.updateAllTableViews();
		}else{
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Ingen bil vald");
			alert.setHeaderText(null);
			alert.setContentText("Du m�ste v�lja en bil att ta bort");
			
			alert.showAndWait();
		}    
	}
	
	public void handleCarSearchTextField(){
		carRepository = new CarRepository();
		mainViewController.carTable.setItems(carRepository.getCarListFromDatabaseForTableView(mainViewController.carSearchTextField.getText()));
	}
}
