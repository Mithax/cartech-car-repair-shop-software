package com.cartech.mainViewTabControllers;

import com.cartech.models.*;
import com.cartech.repository.*;
import com.cartech.viewControllers.MainViewController;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class ServiceTabController {
	
	private MainViewController mainViewController;
	private UserRepository userRepository;
	private CarRepository carRepository;
	private ServiceRepository serviceRepository;
	
	public ServiceTabController(MainViewController mainViewController){
		this.mainViewController = mainViewController;
	}
	
	public void runOnMainViewFXMLInit(){
		mainViewController.serviceOwnerColumn.setCellValueFactory(cellData -> cellData.getValue().getServiceOwnerProperty());
		mainViewController.serviceCarColumn.setCellValueFactory(cellData -> cellData.getValue().getServiceCarProperty());
		mainViewController.serviceDateColumn.setCellValueFactory(cellData -> cellData.getValue().getServiceDateProperty());
		mainViewController.serviceTimeColumn.setCellValueFactory(cellData -> cellData.getValue().getServiceTimeProperty());
		
		showServiceDetailsDialog(null);
		
		mainViewController.serviceTable.getSelectionModel().selectedItemProperty().addListener(
	            (observable, oldValue, newValue) -> showServiceDetailsDialog(newValue));		
	}
	
	private void showServiceDetailsDialog(ServiceModel serviceModel){
		if(serviceModel != null){
			
			carRepository = new CarRepository();
			userRepository = new UserRepository();
			
			CarModel carModel = new CarModel();
			UserModel userModel = new UserModel();
			
			carModel = carRepository.getCarFromDataBaseWithCarID(serviceModel.getCarToBeServedID());		
			userModel = userRepository.getUserFromDataBaseWithUserID(carModel.getOwnerID());
			
			mainViewController.serviceIDLabel.setText(serviceModel.getID().toString());
			mainViewController.serviceDateLabel.setText(serviceModel.getServiceDate().toString());
			mainViewController.serviceTimeLabel.setText(serviceModel.getServiceTime());
			mainViewController.serviceOwnerFirstAndLastNameLabel.setText(userModel.getFirstName() + " " +userModel.getLastName());
			mainViewController.serviceOwnerPhoneNumberLabel.setText(userModel.getPhoneNumber());
			mainViewController.serviceCarBrandLabel.setText(carModel.getBrand());
			mainViewController.serviceCarModelLabel.setText(carModel.getModel());
			mainViewController.serviceCarLicenceplateLabel.setText(carModel.getLicencePlate());
			mainViewController.serviceCommentsTextArea.setText(serviceModel.getComments());
		}else{
			mainViewController.serviceIDLabel.setText("");
			mainViewController.serviceDateLabel.setText("");
			mainViewController.serviceTimeLabel.setText("");
			mainViewController.serviceOwnerFirstAndLastNameLabel.setText("");
			mainViewController.serviceOwnerPhoneNumberLabel.setText("");
			mainViewController.serviceCarBrandLabel.setText("");
			mainViewController.serviceCarModelLabel.setText("");
			mainViewController.serviceCarLicenceplateLabel.setText("");
			mainViewController.serviceCommentsTextArea.setText("");
		}				
	}
	
	public void handleAddServiceButton(){
		ServiceModel newService = new ServiceModel();
		boolean okClicked = mainViewController.mainApplication.showServiceEditOrAddDialog(newService);
		if (okClicked) {
			serviceRepository = new ServiceRepository();
			serviceRepository.addServiceToDatabase(newService);
			mainViewController.updateAllTableViews();			
		}
	}
	
	public void handleEditServiceButton(){
		ServiceModel selectedService = mainViewController.serviceTable.getSelectionModel().getSelectedItem();
		if (selectedService != null) {
			boolean okClicked = mainViewController.mainApplication.showServiceEditOrAddDialog(selectedService);
			if (okClicked) {
				serviceRepository = new ServiceRepository();
				serviceRepository.updateServiceInDatabase(selectedService);
				mainViewController.updateAllTableViews();
			}
		}else{
			Alert alert = new Alert(AlertType.WARNING);
	        alert.initOwner(mainViewController.mainApplication.getPrimaryStage());
	        alert.setTitle("Ingen service vald");
	        alert.setHeaderText(null);
	        alert.setContentText("Du m�ste v�lja en service att �ndra");

	        alert.showAndWait();
		}
	}
	
	public void handleDeleteServiceButton(){
		ServiceModel selectedService = mainViewController.serviceTable.getSelectionModel().getSelectedItem();
		if (selectedService != null) {
			serviceRepository = new ServiceRepository();
			serviceRepository.deleteServiceInDatabase(selectedService);
			mainViewController.updateAllTableViews();
		}else{
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Ingen service vald");
			alert.setHeaderText(null);
			alert.setContentText("Du m�ste v�lja en service att ta bort");
			
			alert.showAndWait();
		}    
	}
	
	public void handleServiceSearchTextField(){
		serviceRepository = new ServiceRepository();
		mainViewController.serviceTable.setItems(serviceRepository.getServiceListFromDatabaseForTableView(mainViewController.serviceSearchTextField.getText()));
	}
}
