package com.cartech.models;

import java.time.LocalDate;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class CarModel {
	
	private Long ID;
	private Long ownerID;
	
	private String brand;
	private String model;
	private String licencePlate;
	private String comments;
	
	private LocalDate carAddedToApplication;
		
	private StringProperty brandProperty;
    private StringProperty modelProperty;
    private StringProperty licencePlateProperty;
	
	
	public CarModel(){
		carAddedToApplication = LocalDate.now();
	}

	public Long getID() {
		return ID;
	}

	public void setID(Long iD) {
		ID = iD;
	}
	
	public Long getOwnerID() {
		return ownerID;
	}

	public void setOwnerID(Long ownerID) {
		this.ownerID = ownerID;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getLicencePlate() {
		return licencePlate;
	}

	public void setLicencePlate(String licencePlate) {
		this.licencePlate = licencePlate;
	}

	public StringProperty getBrandProperty() {
		brandProperty = new SimpleStringProperty(brand);
		return brandProperty;
	}

	public StringProperty getModelProperty() {
		modelProperty = new SimpleStringProperty(model);
		return modelProperty;
	}

	public StringProperty getLicencePlateProperty() {
		licencePlateProperty = new SimpleStringProperty(licencePlate);
		return licencePlateProperty;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public LocalDate getCarAddedToApplication() {
		return carAddedToApplication;
	}

	public void setCarAddedToApplication(LocalDate carAddedToApplication) {
		this.carAddedToApplication = carAddedToApplication;
	}

	@Override
	public String toString() {
		return brand + " " + model + " " + licencePlate;
	}
}
	
