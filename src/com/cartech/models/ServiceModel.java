package com.cartech.models;

import java.time.LocalDate;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ServiceModel {
	
	private Long ID;
	private Long carToBeServedID;

	private LocalDate serviceDate;
	private String serviceTimeHour;
	private String serviceTimeMinute;
	private String comments;
	
	private StringProperty serviceDateProperty;
	private StringProperty serviceTimeProperty;
	private StringProperty serviceOwnerProperty;
	private StringProperty serviceCarProperty;
	
	public ServiceModel(){
		
	}

	public Long getID() {
		return ID;
	}

	public void setID(Long iD) {
		ID = iD;
	}

	public LocalDate getServiceDate() {
		return serviceDate;
	}

	public void setServiceDate(LocalDate serviceDate) {
		this.serviceDate = serviceDate;
	}

	public String getServiceTime() {
		return serviceTimeHour + ":" + serviceTimeMinute;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Long getCarToBeServedID() {
		return carToBeServedID;
	}

	public void setCarToBeServedID(Long carToBeServedID) {
		this.carToBeServedID = carToBeServedID;
	}

	public StringProperty getServiceDateProperty() {
		serviceDateProperty = new SimpleStringProperty(serviceDate.toString());
		return serviceDateProperty;
	}

	public StringProperty getServiceTimeProperty() {
		serviceTimeProperty = new SimpleStringProperty(getServiceTime());
		return serviceTimeProperty;
	}	
	
	public StringProperty getServiceOwnerProperty() {
		return serviceOwnerProperty;
	}

	public StringProperty getServiceCarProperty() {
		return serviceCarProperty;
	}
	
	public void setServiceOwnerProperty(String serviceOwner) {
		serviceOwnerProperty = new SimpleStringProperty(serviceOwner);
	}

	public void setServiceCarProperty(String serviceCar) {
		serviceCarProperty = new SimpleStringProperty(serviceCar);
	}

	public String getServiceTimeHour() {
		return serviceTimeHour;
	}

	public void setServiceTimeHour(String serviceTimeHour) {
		this.serviceTimeHour = serviceTimeHour;
	}

	public String getServiceTimeMinute() {
		return serviceTimeMinute;
	}

	public void setServiceTimeMinute(String serviceTimeMinute) {
		this.serviceTimeMinute = serviceTimeMinute;
	}	

	@Override
	public String toString() {
		return serviceDate.toString() + " " + getServiceTime();
	}
	
	
}
