package com.cartech.models;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class UserModel {   

    private Long ID;
    
    private String firstName;
    private String lastName;
    private String socialSecurityNumber;
    private String streetAdress;
    private String zipCode;
    private String city;
    private String phoneNumber;
    private String email;
    private String username;
    private String password;
    private boolean employee;
    
    private StringProperty firstNameProperty;
    private StringProperty lastNameProperty;
    private StringProperty phoneNumberProperty;
    
    public UserModel(){
        
    }

	public Long getID() {
		return ID;
	}
	
	public void setID(Long iD) {
		ID = iD;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSocialSecurityNumber() {
		return socialSecurityNumber;
	}

	public void setSocialSecurityNumber(String socialSecurityNumber) {
		this.socialSecurityNumber = socialSecurityNumber;
	}

	public String getStreetAdress() {
		return streetAdress;
	}

	public void setStreetAdress(String streetAdress) {
		this.streetAdress = streetAdress;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isEmployee() {
		return employee;
	}

	public void setEmployee(boolean employee) {
		this.employee = employee;
	}

	public StringProperty getFirstNameProperty() {
		firstNameProperty = new SimpleStringProperty(firstName);
		return firstNameProperty;
	}

	public StringProperty getLastNameProperty() {
		lastNameProperty = new SimpleStringProperty(lastName);
		return lastNameProperty;
	}
	
	public StringProperty getPhoneNumberProperty() {
		phoneNumberProperty = new SimpleStringProperty(phoneNumber);
		return phoneNumberProperty;
	}

	@Override
	public String toString() {
		return firstName + " " + lastName + " " + socialSecurityNumber + " " + streetAdress + " " 
		+ zipCode + " " + " " + city + " " + phoneNumber + " " + email + " " + username;
	}	
}