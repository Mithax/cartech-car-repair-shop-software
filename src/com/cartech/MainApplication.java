package com.cartech;

import java.io.IOException;
import com.cartech.models.CarModel;
import com.cartech.models.ServiceModel;
import com.cartech.models.UserModel;
import com.cartech.viewControllers.*;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class MainApplication extends Application {
	
	private Stage primaryStage;
	private BorderPane rootLayout;
	private RootController rootController;
	public UserModel currentUser;

	
	@Override
	public void start(Stage primaryStage) {
		
		//Necessary to avoid freezing the program when interacting with JavaFX ComboBox
		//"The bug appears to have been introduced in JDK 8u40, and affects Windows 10 systems with a touchscreen installed and enabled"
		System.setProperty("glass.accessible.force", "false");
				
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("Car Tech");
		
		loadRootLayout();
		showLoginView();
	}

	public static void main(String[] args) {
		launch(args);
	}
	
	public void loadRootLayout(){
		try {
			
		FXMLLoader fxmlLoader = new FXMLLoader();
		fxmlLoader.setLocation(MainApplication.class.getResource("/com/cartech/views/RootView.fxml"));
		rootLayout = (BorderPane) fxmlLoader.load();
		
		Scene scene = new Scene(rootLayout);
		primaryStage.setScene(scene);
		
		rootController = fxmlLoader.getController();
		rootController.setMainApplication(this);
		
		primaryStage.show();
		
		} catch (IOException exception) {
			System.out.println("IOException in MainApplication.loadRootLayout: " + exception.getMessage());
		}		
	}
	
	public void showLoginView(){
		try {
			
		FXMLLoader fxmlLoader = new FXMLLoader();
		fxmlLoader.setLocation(MainApplication.class.getResource("/com/cartech/views/LoginView.fxml"));
		AnchorPane loginView = (AnchorPane) fxmlLoader.load();
		
		rootLayout.setCenter(loginView);
		
		LoginController loginController = fxmlLoader.getController();
	    loginController.setMainApplication(this);
	    
		
		} catch (IOException exception) {
			System.out.println("IOException in MainApplication.showMainView: " + exception.getMessage());
		}	
	}
	
	public void showMainView(UserModel currentUser){
		this.currentUser = currentUser;
		try {
			
		FXMLLoader fxmlLoader = new FXMLLoader();
		fxmlLoader.setLocation(MainApplication.class.getResource("/com/cartech/views/MainView.fxml"));
		AnchorPane mainView = (AnchorPane) fxmlLoader.load();
		
		rootLayout.setCenter(mainView);
		
		MainViewController mainViewController = fxmlLoader.getController();
	    mainViewController.setMainApplication(this);
	     
	    rootController.setMainViewController(mainViewController);
	    rootController.showMenuItemsOnAdminLogin();
		
		} catch (IOException exception) {
			System.out.println("IOException in MainApplication.showMainView: " + exception.getMessage());
			exception.printStackTrace();
		}	
	}
	
	public void showCustomerView(UserModel currentUser){
		this.currentUser = currentUser;
		try {
			
		FXMLLoader fxmlLoader = new FXMLLoader();
		fxmlLoader.setLocation(MainApplication.class.getResource("/com/cartech/views/CustomerLoginView.fxml"));
		AnchorPane customerView = (AnchorPane) fxmlLoader.load();
		
		rootLayout.setCenter(customerView);
		
		CustomerLoginController customerLoginController = fxmlLoader.getController();
		customerLoginController.setandShowCurrentUser(currentUser);
		
		rootController.showMenuItemsOnCustomerLogin();
		
		} catch (IOException exception) {
			System.out.println("IOException in MainApplication.showMainView: " + exception.getMessage());
			exception.printStackTrace();
		}	
	}
	
	public Stage getPrimaryStage(){
		return primaryStage;
	}
	
	public boolean showUserEditOrAddDialog(UserModel userModel){
		try{
			FXMLLoader fxmlLoader = new FXMLLoader();
			fxmlLoader.setLocation(MainApplication.class.getResource("/com/cartech/views/EditOrAddUserDialogView.fxml"));
	        AnchorPane page = (AnchorPane) fxmlLoader.load();
	        
	        Stage dialogStage = new Stage();
	        dialogStage.setTitle("�ndra anv�ndare");
	        dialogStage.initModality(Modality.WINDOW_MODAL);
	        dialogStage.initOwner(primaryStage);
	        Scene scene = new Scene(page);
	        dialogStage.setScene(scene);
	        
	        EditOrAddUserDialogController editOrAddUserDialogController = fxmlLoader.getController();
	        editOrAddUserDialogController.setDialogStage(dialogStage);
	        editOrAddUserDialogController.setUser(userModel);
	        
	        dialogStage.showAndWait();
	        
	        return editOrAddUserDialogController.isOkClicked();
	        
		}catch(IOException exception){
			System.out.println("IOException in MainApplication.showUserEditOrAddDialog: " + exception.getMessage());
	        return false;
		}
	}
	
	public boolean showCarEditOrAddDialog(CarModel carModel){
		try{
			FXMLLoader fxmlLoader = new FXMLLoader();
			fxmlLoader.setLocation(MainApplication.class.getResource("/com/cartech/views/EditOrAddCarDialogView.fxml"));
	        AnchorPane page = (AnchorPane) fxmlLoader.load();
	        
	        Stage dialogStage = new Stage();
	        dialogStage.setTitle("�ndra bil");
	        dialogStage.initModality(Modality.WINDOW_MODAL);
	        dialogStage.initOwner(primaryStage);
	        Scene scene = new Scene(page);
	        dialogStage.setScene(scene);
	        
	        EditOrAddCarDialogController editOrAddCarDialogController = fxmlLoader.getController();
	        editOrAddCarDialogController.setDialogStage(dialogStage);
	        editOrAddCarDialogController.setCar(carModel);
	        
	        dialogStage.showAndWait();
	        
	        return editOrAddCarDialogController.isOkClicked();
	        
		}catch(IOException exception){
			System.out.println("IOException in MainApplication.showCarEditOrAddDialog: " + exception.getMessage());
	        return false;
		}
	}
	
	public boolean showServiceEditOrAddDialog(ServiceModel serviceModel){
		try{
			FXMLLoader fxmlLoader = new FXMLLoader();
			fxmlLoader.setLocation(MainApplication.class.getResource("/com/cartech/views/EditOrAddServiceDialogView.fxml"));
	        AnchorPane page = (AnchorPane) fxmlLoader.load();
	        
	        Stage dialogStage = new Stage();
	        dialogStage.setTitle("�ndra service");
	        dialogStage.initModality(Modality.WINDOW_MODAL);
	        dialogStage.initOwner(primaryStage);
	        Scene scene = new Scene(page);
	        dialogStage.setScene(scene);
	        
	        EditOrAddServiceDialogController editOrAddServiceDialogController = fxmlLoader.getController();
	        editOrAddServiceDialogController.setDialogStage(dialogStage);
	        editOrAddServiceDialogController.setService(serviceModel);
	        
	        dialogStage.showAndWait();
	        
	        return editOrAddServiceDialogController.isOkClicked();
	        
		}catch(IOException exception){
			System.out.println("IOException in MainApplication.showServiceEditOrAddDialog: " + exception.getMessage());
			return false;
		}
	}
	
	public void showServiceStatisticsDialog(int statisticsChartToLoad) {
		int servicesOfCurrentAndPreviousYear = 0;
		int servicesOfCurrentAndPreviousMonth = 1;
		int averageServiceYearPerBrand = 2;
		
	    try {
	    			
	        FXMLLoader fxmlLoader = new FXMLLoader();
	        fxmlLoader.setLocation(MainApplication.class.getResource("/com/cartech/views/StatisticsChartView.fxml"));
	        AnchorPane page = (AnchorPane) fxmlLoader.load();
	        Stage dialogStage = new Stage();
	        dialogStage.setTitle("Serivce Statistic");
	        dialogStage.initModality(Modality.WINDOW_MODAL);
	        dialogStage.initOwner(primaryStage);
	        Scene scene = new Scene(page);
	        dialogStage.setScene(scene);
	        
	        StatisticChartController statisticChartController = fxmlLoader.getController();
	        
	        if (statisticsChartToLoad == servicesOfCurrentAndPreviousYear) {
				statisticChartController.setServicesOfCurrentAndPreviousYear();
			} else if (statisticsChartToLoad == servicesOfCurrentAndPreviousMonth) {
				statisticChartController.setServicesOfCurrentAndPreviousMonth();
			} else if (statisticsChartToLoad == averageServiceYearPerBrand) {
				statisticChartController.setAverageServiceYearPerBrand();
			}

	        dialogStage.show();

	    } catch (IOException exception) {
	    	System.out.println("IOException in MainApplication.showBirthdayStatistics: "+ exception.getMessage());
	    	exception.printStackTrace();
	    }
	}
	
	public void showChangePassWordDialog(){
		try{
			FXMLLoader fxmlLoader = new FXMLLoader();
			fxmlLoader.setLocation(MainApplication.class.getResource("/com/cartech/views/ChangePassword.fxml"));
	        AnchorPane page = (AnchorPane) fxmlLoader.load();
	        
	        Stage dialogStage = new Stage();
	        dialogStage.setTitle("�ndra l�senord");
	        dialogStage.initModality(Modality.WINDOW_MODAL);
	        dialogStage.initOwner(primaryStage);
	        Scene scene = new Scene(page);
	        dialogStage.setScene(scene);
	        
	        ChangePasswordController changePasswordController = fxmlLoader.getController();
	        changePasswordController.setDialogStage(dialogStage);
	        changePasswordController.setCurrentUser(currentUser);
	        
	        dialogStage.showAndWait();
	        
		}catch(IOException exception){
			System.out.println("IOException in MainApplication.showUserEditOrAddDialog: " + exception.getMessage());
		}
	}
}
