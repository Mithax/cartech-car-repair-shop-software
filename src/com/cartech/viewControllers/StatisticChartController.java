package com.cartech.viewControllers;

import java.text.DateFormatSymbols;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.*;


import com.cartech.models.*;
import com.cartech.repository.*;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.*;

public class StatisticChartController {
	
	@FXML
    private BarChart<String, Number> servicesChart;

    @FXML
    private CategoryAxis categoryXAxis;
    
    private ServiceRepository serviceRepository; 
    private CarRepository carRepository;
    private List<ServiceModel> serviceList;
    private List<CarModel> carList;

    
    @FXML
    private void initialize() {   	
    	getServiceListFromDataBase(); 
    	getCarListFromDataBase();
    }
  
    private void getServiceListFromDataBase(){
    	serviceRepository = new ServiceRepository();
    	serviceList = serviceRepository.getServiceListFromDatabaseForTableView("");
    }
    private void getCarListFromDataBase(){
    	carRepository = new CarRepository();
    	carList = carRepository.getCarListFromDatabaseForTableView("");
    }

    public void setServicesOfCurrentAndPreviousYear() {
    	categoryXAxis.setLabel("M�nad");
    	
    	LocalDate today = LocalDate.now();
    	LocalDate previousYear = LocalDate.now().minus(1, ChronoUnit.YEARS);
    	
    	//Gets names of each months and puts in a list that will be displayed in X-Axis
        ObservableList<String> monthNames = FXCollections.observableArrayList();
        monthNames.clear(); 
        
        String[] monthsNamesForList = DateFormatSymbols.getInstance(Locale.ENGLISH).getShortMonths();       
        monthNames.addAll(Arrays.asList(monthsNamesForList));
        
        //Arrays with one double for each month that are counted in a forloop with a list of all services
        double[] monthCounterThisYear = new double[monthNames.size()];
        double[] monthCounterPreviousYear = new double[monthNames.size()];
        
    	for (int i = 0; i < serviceList.size(); i++) {  		
    		if (serviceList.get(i).getServiceDate().getYear() == today.getYear()) {  
    			int monthToCount = serviceList.get(i).getServiceDate().getMonthValue() - 1;
    			monthCounterThisYear[monthToCount]++;
			}else if(serviceList.get(i).getServiceDate().getYear() == previousYear.getYear()){
				int monthToCount = serviceList.get(i).getServiceDate().getMonthValue() - 1;
				monthCounterPreviousYear[monthToCount]++;
			}
		}
    	populateServiceChart(monthCounterPreviousYear, monthNames, Integer.toString(previousYear.getYear()));
    	populateServiceChart(monthCounterThisYear, monthNames, Integer.toString(today.getYear()));  	
    }
    
    public void setServicesOfCurrentAndPreviousMonth() {
    	categoryXAxis.setLabel("Dag");
    	
    	LocalDate today =  LocalDate.now();
    	LocalDate previousMonth = LocalDate.now().minus(1, ChronoUnit.MONTHS);
    	 
    	ObservableList<String> dayNames = FXCollections.observableArrayList();
    	dayNames.clear();
    	
    	int maximumDaysInMonth = 31;
    	for (int i = 1; i <= maximumDaysInMonth; i++) {
			dayNames.add(Integer.toString(i));
		}   
    	
    	double[] dayCounterThisMonth = new double[maximumDaysInMonth];
        double[] dayCounterPreviousMonth = new double[maximumDaysInMonth];
         
    	for (int i = 0; i < serviceList.size(); i++) {   		
    		if (serviceList.get(i).getServiceDate().getMonthValue() == today.getMonthValue() && serviceList.get(i).getServiceDate().getYear() == today.getYear()) {  
    			int dayToCount = serviceList.get(i).getServiceDate().getDayOfMonth() - 1;
    			dayCounterThisMonth[dayToCount]++;
			}else if(serviceList.get(i).getServiceDate().getMonthValue() == previousMonth.getMonthValue() && serviceList.get(i).getServiceDate().getYear() == previousMonth.getYear()){
				int dayToCount = serviceList.get(i).getServiceDate().getDayOfMonth() - 1;
				dayCounterPreviousMonth[dayToCount]++;
			}
		}
    	populateServiceChart(dayCounterPreviousMonth, dayNames, previousMonth.getMonth().toString());
    	populateServiceChart(dayCounterThisMonth, dayNames, today.getMonth().toString());
    	
    }
    
    public void setAverageServiceYearPerBrand(){
    	categoryXAxis.setLabel("M�rke");
    	String chartSeriesName = "Bilm�rke";
   	 
    	ObservableList<String> brandNames = FXCollections.observableArrayList();
    	brandNames.clear();
    	
    	List<String> brandsOfAllCarsInDB = new ArrayList<>();
    	for (int i = 0; i < carList.size(); i++) {
    		brandsOfAllCarsInDB.add(carList.get(i).getBrand());
		}
    	
    	//Using set to filter out only unique brands
    	Set<String> uniqueCars = new HashSet<String>(brandsOfAllCarsInDB);   	
    	brandNames.addAll(uniqueCars);
    	
    	double[] serviceCounter = new double[brandNames.size()];
    	
    	for (int i = 0; i < carList.size(); i++) {
    		double carsTotalYearsInSystem = ChronoUnit.YEARS.between(carList.get(i).getCarAddedToApplication(), LocalDate.now());
    		double servicesPerformed = serviceRepository.getServiceListFromDataBaseForTableViewWithCarID(carList.get(i).getID()).size();
    		double servicesPerYear = servicesPerformed / carsTotalYearsInSystem;
    		
    		for (int j = 0; j < brandNames.size(); j++) {
				if (carList.get(i).getBrand().equalsIgnoreCase(brandNames.get(j))) {
					if (serviceCounter[j] != 0) {
						serviceCounter[j] = (serviceCounter[j] +servicesPerYear) / 2;
					}else{
						serviceCounter[j] = servicesPerYear;
					}
				}
			}
		}
    	populateServiceChart(serviceCounter, brandNames, chartSeriesName);     	
    }
      
    private  void populateServiceChart(double[] yAxisData, ObservableList<String> xAxisData, String seriesName){   	
    	XYChart.Series<String, Number> statisticSeries = new XYChart.Series<>();
    	
    	statisticSeries.setName(seriesName);
    	
        for (int i = 0; i < yAxisData.length; i++) {
            statisticSeries.getData().add(new XYChart.Data<>(xAxisData.get(i), yAxisData[i]));
        }       
        servicesChart.getData().add(statisticSeries);   	
    }
}
