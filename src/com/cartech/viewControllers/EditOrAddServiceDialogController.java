package com.cartech.viewControllers;

import com.cartech.models.*;
import com.cartech.repository.*;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

public class EditOrAddServiceDialogController {
	
	@FXML
	private TextField searchTF;
	
	@FXML
	private DatePicker editDateDP;
	
	@FXML
	private TableView<CarModel> editServiceCarTW;
	
	@FXML
	private TableColumn<CarModel, String> brandTC, modelTC, licenceNumberTC;
	
	@FXML
	private TextArea editCommentsTA;
	
	@FXML
	private ComboBox<String> editHourCB, editMinCB;
	
	private CarRepository carRepository;
	private UserRepository userRepository;
	
	private Stage dialogStage;
	private ServiceModel serviceModel;
	private boolean okClicked = false;


	public void setDialogStage(Stage dialogStage){
		this.dialogStage = dialogStage;
	}
	
	@FXML
	private void initialize(){
		
		brandTC.setCellValueFactory(cellData -> cellData.getValue().getBrandProperty());
		modelTC.setCellValueFactory(cellData -> cellData.getValue().getModelProperty());
		licenceNumberTC.setCellValueFactory(cellData -> cellData.getValue().getLicencePlateProperty());
		
		handleSearchTextField();
		populateTimeComboBoxes();
	}
	
	private void populateTimeComboBoxes(){

			ObservableList<String> hoursList = FXCollections.observableArrayList();
			ObservableList<String> minutesList = FXCollections.observableArrayList();
			
			for (Integer i = 0; i < 24; i++) {
				if (i < 10) {
					hoursList.add("0" + i.toString());
				}else{
					hoursList.add(i.toString());
				}
			}
			for (Integer i = 0; i < 60; i += 5) {
				if (i < 10) {
					minutesList.add("0" + i.toString());
				}else{
					minutesList.add(i.toString());
				}
			}
			editHourCB.setItems(hoursList);
			editMinCB.setItems(minutesList);	
	}
		
	public void setService(ServiceModel serviceModel){
		this.serviceModel = serviceModel;
		if (serviceModel.getID() != null) {
			carRepository = new CarRepository();
			editServiceCarTW.setItems(carRepository.getCarListFromDataBaseForTableViewWithCarID(serviceModel.getCarToBeServedID()));
			editServiceCarTW.getSelectionModel().selectFirst();
			editDateDP.setValue(serviceModel.getServiceDate());
			editHourCB.setValue(serviceModel.getServiceTimeHour());
			editMinCB.setValue(serviceModel.getServiceTimeMinute());
			editCommentsTA.setText(serviceModel.getComments());
		}		
	}
	
	public boolean isOkClicked(){
        return okClicked;
    }
	
	@FXML
	public void handleSearchTextField(){
		carRepository = new CarRepository();
		editServiceCarTW.setItems(carRepository.getCarListFromDatabaseForTableView(searchTF.getText()));
	}
	
	@FXML
	private void handleOk(){
		if (isInputsValid()) {
			userRepository = new UserRepository();
			CarModel carModel = editServiceCarTW.getSelectionModel().getSelectedItem();
			
			serviceModel.setCarToBeServedID(carModel.getID());
			serviceModel.setServiceDate(editDateDP.getValue());
			serviceModel.setServiceTimeHour(editHourCB.getValue());
			serviceModel.setServiceTimeMinute(editMinCB.getValue());
			serviceModel.setComments(editCommentsTA.getText());
			serviceModel.setServiceCarProperty(carModel.getLicencePlate());
			serviceModel.setServiceOwnerProperty(userRepository.getUserFromDataBaseWithUserID(carModel.getOwnerID()).getFirstName()
			+ " " + userRepository.getUserFromDataBaseWithUserID(carModel.getOwnerID()).getLastName());
			
			okClicked = true;
			dialogStage.close();
		}		
	}
	
	@FXML
	private void handleCancel(){
		dialogStage.close();
	}
	
	private boolean isInputsValid(){
		String errorMessage = "";
		
		if (editServiceCarTW.getSelectionModel().getSelectedItem() == null) {
			errorMessage += "Ingen bil vald\n";
		}
		if (editDateDP.getValue() == null) {
			errorMessage += "Inget datum valt\n";
		}
		if (editHourCB.getValue() == null || editHourCB.getValue().equals("")) {
			errorMessage += "Timma ej vald\n";
		}
		if (editMinCB.getValue() == null || editMinCB.getValue().equals("")) {
			errorMessage += "Minut ej valt\n";
		}
		
		if (errorMessage.length() == 0) {
			return true;
		}else{
			Alert alert = new Alert(AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Ogiltiga f�lt");
            alert.setHeaderText(null);
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
		}
	}	
}
