package com.cartech.viewControllers;

import com.cartech.models.*;
import com.cartech.repository.*;

import javafx.fxml.FXML;
import javafx.scene.control.*;

public class CustomerLoginController {
	
	@FXML
	private Label firstNameLabel, lastNameLabel, socialSecurityNumberLabel, streetAdressLabel, zipCodeLabel, cityLabel, phoneNumberLabel,
	emailLabel, userNameLabel, carBrandLabel, carModelLabel, carLicencePlateLabel, serviceTimeLabel, serviceDateLabel;
	
	@FXML
	private TableView<CarModel> carTable;
	
	@FXML
	private TableColumn<CarModel, String> carBrandTC, carModelTC, carLicencePlateTC;
	
	@FXML
	private TableView<ServiceModel> serviceTable;
	
	@FXML
	private TableColumn<ServiceModel, String> serviceDateTC, serviceTimeTC;
	
	private CarRepository carRepository;
	private ServiceRepository serviceRepository;
	private UserModel currentUser;
	
	@FXML
	private void initialize(){
		
		carBrandTC.setCellValueFactory(cellData -> cellData.getValue().getBrandProperty());
		carModelTC.setCellValueFactory(cellData -> cellData.getValue().getModelProperty());
		carLicencePlateTC.setCellValueFactory(cellData -> cellData.getValue().getLicencePlateProperty());
				
		serviceDateTC.setCellValueFactory(cellData -> cellData.getValue().getServiceDateProperty());
		serviceTimeTC.setCellValueFactory(cellData -> cellData.getValue().getServiceTimeProperty());
		
		carTable.getSelectionModel().selectedItemProperty().addListener(
	            (observable, oldValue, newValue) -> showCarDetails(newValue));
		
		serviceTable.getSelectionModel().selectedItemProperty().addListener(
	            (observable, oldValue, newValue) -> showServiceDetails(newValue));
	}
	
	public void setandShowCurrentUser(UserModel user){
		this.currentUser = user;
		showUsersInfo();
	}


	private void showUsersInfo() {
		
		carRepository = new CarRepository();
		
		firstNameLabel.setText(currentUser.getFirstName());
		lastNameLabel.setText(currentUser.getLastName());
		socialSecurityNumberLabel.setText(currentUser.getSocialSecurityNumber());
		streetAdressLabel.setText(currentUser.getStreetAdress());
		zipCodeLabel.setText(currentUser.getZipCode());
		cityLabel.setText(currentUser.getCity());
		phoneNumberLabel.setText(currentUser.getPhoneNumber());
		emailLabel.setText(currentUser.getEmail());
		userNameLabel.setText(currentUser.getUsername());
		
		carTable.setItems(carRepository.getCarListFromDataBaseForTableViewWithUserID(currentUser.getID()));	
	}
	
	private void showCarDetails(CarModel carModel){
		serviceRepository = new ServiceRepository();
		
		carBrandLabel.setText(carModel.getBrand());
		carModelLabel.setText(carModel.getModel());
		carLicencePlateLabel.setText(carModel.getLicencePlate());

		serviceTable.setItems(serviceRepository.getServiceListFromDataBaseForTableViewWithCarID(carModel.getID()));		
	}
	
	private void showServiceDetails(ServiceModel serviceModel) {
		if (serviceModel != null) {
			serviceTimeLabel.setText(serviceModel.getServiceTime());
			serviceDateLabel.setText(serviceModel.getServiceDate().toString());
		} else{
			serviceTimeLabel.setText("");
			serviceDateLabel.setText("");
		}
	}
}
