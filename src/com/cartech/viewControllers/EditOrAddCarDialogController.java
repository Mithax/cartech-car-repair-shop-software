package com.cartech.viewControllers;

import com.cartech.models.CarModel;
import com.cartech.models.UserModel;
import com.cartech.repository.UserRepository;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

public class EditOrAddCarDialogController {
	
	@FXML
	private TextField searchTF, editBrandTF, editModelTF, editLicencePlateTF; 
	
	@FXML
	private TableView<UserModel> editOwnerTW;
	
	@FXML
	private TableColumn<UserModel, String> firstNameTC, lastNameTC, phoneNumberTC;
	
	@FXML
	private TextArea editCommentsTA;
	
	private UserRepository userRepository;
	
	private Stage dialogStage;
	private CarModel carModel;
	private boolean okClicked = false;


	public void setDialogStage(Stage dialogStage){
		this.dialogStage = dialogStage;
	}
	
	@FXML
	private void initialize(){
		
		firstNameTC.setCellValueFactory(cellData -> cellData.getValue().getFirstNameProperty());
		lastNameTC.setCellValueFactory(cellData -> cellData.getValue().getLastNameProperty());
		phoneNumberTC.setCellValueFactory(cellData -> cellData.getValue().getPhoneNumberProperty());
		
		handleSearchTextField();
	}
		
	public void setCar(CarModel carModel){
		this.carModel = carModel;
		if (carModel.getID() != null) {
			userRepository = new UserRepository();
			editOwnerTW.setItems(userRepository.getUserListFromDataBaseForTableViewWithID(carModel.getOwnerID()));
			editOwnerTW.getSelectionModel().selectFirst();
			editBrandTF.setText(carModel.getBrand());
			editModelTF.setText(carModel.getModel());
			editLicencePlateTF.setText(carModel.getLicencePlate());
			editCommentsTA.setText(carModel.getComments());
		}		
	}
	
	public boolean isOkClicked(){
        return okClicked;
    }
	
	@FXML
	public void handleSearchTextField(){
		userRepository = new UserRepository();
		editOwnerTW.setItems(userRepository.getUserListFromDatabaseForTableView(searchTF.getText()));
	}
	
	@FXML
	private void handleOk(){
		if (isInputsValid()) {
			UserModel userModel = editOwnerTW.getSelectionModel().getSelectedItem();
			
			carModel.setOwnerID(userModel.getID());
			carModel.setBrand(editBrandTF.getText());
			carModel.setModel(editModelTF.getText());
			carModel.setLicencePlate(editLicencePlateTF.getText());
			carModel.setComments(editCommentsTA.getText());
			
			okClicked = true;
			dialogStage.close();
		}		
	}
	
	@FXML
	private void handleCancel(){
		dialogStage.close();
	}
	
	private boolean isInputsValid(){
		String errorMessage = "";
		
		if (editOwnerTW.getSelectionModel().getSelectedItem() == null) {
			errorMessage += "Ingen �gare vald\n";
		}
		if (editBrandTF.getText() == null || editBrandTF.getText().equals("")) {
			errorMessage += "Ogiltigt M�rke\n";
		}
		if (editModelTF.getText() == null || editModelTF.getText().equals("")) {
			errorMessage += "Ogiltig modell\n";
		}
		if (editLicencePlateTF.getText() == null || editLicencePlateTF.getText().equals("")) {
			errorMessage += "Ogiltigt Regnummer\n";
		}
		
		if (errorMessage.length() == 0) {
			return true;
		}else{
			Alert alert = new Alert(AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Ogiltiga f�lt");
            alert.setHeaderText(null);
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
		}
	}	
}
