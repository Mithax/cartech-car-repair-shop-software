package com.cartech.viewControllers;

import com.cartech.MainApplication;
import com.cartech.models.UserModel;
import com.cartech.repository.UserRepository;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;

public class LoginController {
	
	@FXML
	private TextField userNameTextField, passwordField;
	
	private MainApplication mainApplication;
	private UserRepository userRepository = new UserRepository();
	
	public void setMainApplication(MainApplication mainApplication){
		this.mainApplication = mainApplication;
	}
	
	@FXML
	private void handleLogin(){
		if (isInputsValid()) {
			
			UserModel currentUser = userRepository.getUserFromDataBaseWithUserName(userNameTextField.getText());
			
			if (currentUser.getID() != null) {
				if (currentUser.getPassword().equals(passwordField.getText())) {
					if (currentUser.isEmployee()) {
						mainApplication.showMainView(currentUser);
					}else{
						mainApplication.showCustomerView(currentUser);
					}
				}else{
					Alert alert = new Alert(AlertType.ERROR);
		            alert.initOwner(mainApplication.getPrimaryStage());
		            alert.setTitle("Fel l�senord");
		            alert.setHeaderText(null);
		            alert.setContentText("L�senordet matchar ej " + userNameTextField.getText());

		            alert.showAndWait();
				}
			} else{
				Alert alert = new Alert(AlertType.ERROR);
	            alert.initOwner(mainApplication.getPrimaryStage());
	            alert.setTitle("Ingen anv�ndare");
	            alert.setHeaderText(null);
	            alert.setContentText("Ingen anv�ndare med " + userNameTextField.getText() + " hittades" );

	            alert.showAndWait();
			}
		}
	}
	
	private boolean isInputsValid(){
		String errorMessage = "";
		
		if (userNameTextField.getText().equals("") || userNameTextField.getText() == null) {
			errorMessage += "Inget anv�ndarnamn angivet\n";
		}
		if (passwordField.getText().equals("") || passwordField.getText() == null) {
			errorMessage += "Inget l�senord angivet\n";
		}
		
		if (errorMessage.length() == 0) {
			return true;
		}else{
			Alert alert = new Alert(AlertType.ERROR);
            alert.initOwner(mainApplication.getPrimaryStage());
            alert.setTitle("Ogiltiga f�lt");
            alert.setHeaderText(null);
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
		}
	}	
}


