package com.cartech.viewControllers;

import com.cartech.models.UserModel;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

public class EditOrAddUserDialogController {
	
	@FXML
	private TextField editFirstNameTF, editLastNameTF, editSocialSecurityTF, editStreetAdressTF,
	editZipCodeTF, editCityTF, editPhoneNumberTF, editEmailTF, editUsernameTF; 
	
	@FXML
	private CheckBox editEmployeeCB;
	
	@FXML
	private PasswordField editPasswordPF;
	
	public EditOrAddUserDialogController(){
		
	}
	
	private Stage dialogStage;
	private UserModel userModel;
	private boolean okClicked = false;
	
	public void setDialogStage(Stage dialogStage){
		this.dialogStage = dialogStage;
	}
	
	public void setUser(UserModel userModel){
		this.userModel = userModel;
		if (userModel.getID() != null) {
			editFirstNameTF.setText(userModel.getFirstName());
			editLastNameTF.setText(userModel.getLastName());
			editSocialSecurityTF.setText(userModel.getSocialSecurityNumber());
			editStreetAdressTF.setText(userModel.getStreetAdress());
			editZipCodeTF.setText(userModel.getZipCode());
			editCityTF.setText(userModel.getCity());
			editPhoneNumberTF.setText(userModel.getPhoneNumber());
			editEmailTF.setText(userModel.getEmail());
			editUsernameTF.setText(userModel.getUsername());
			editPasswordPF.setText(userModel.getPassword());
			if (userModel.isEmployee()) {
				editEmployeeCB.setSelected(true);
				editPasswordPF.setDisable(true);
			}else{
				editEmployeeCB.setSelected(false);
			}	
		}	 
	}
	
	public boolean isOkClicked(){
        return okClicked;
    }
	
	@FXML
	private void handleOk(){
		if (isInputsValid()) {
			userModel.setFirstName(editFirstNameTF.getText());
			userModel.setLastName(editLastNameTF.getText());
			userModel.setSocialSecurityNumber(editSocialSecurityTF.getText());
			userModel.setStreetAdress(editStreetAdressTF.getText());
			userModel.setZipCode(editZipCodeTF.getText());
			userModel.setCity(editCityTF.getText());
			userModel.setPhoneNumber(editPhoneNumberTF.getText());
			userModel.setEmail(editEmailTF.getText());
			userModel.setUsername(editUsernameTF.getText());
			userModel.setPassword(editPasswordPF.getText());
			boolean employee;
			if (editEmployeeCB.isSelected()) {
				employee = true;
			}else{
				employee = false;
			}
			userModel.setEmployee(employee);
			
			okClicked = true;
			dialogStage.close();
		}		
	}
	
	@FXML
	private void handleCancel(){
		dialogStage.close();
	}
	
	private boolean isInputsValid(){
		String errorMessage = "";
		
		if (editFirstNameTF.getText() == null || editFirstNameTF.getText().equals("")) {
			errorMessage += "Ogiltigt f�rnamn\n";
		}
		if (editLastNameTF.getText() == null || editLastNameTF.getText().equals("")) {
			errorMessage += "Ogiltigt efternamn\n";
		}
		if (editSocialSecurityTF.getText() == null || editSocialSecurityTF.getText().equals("")) {
			errorMessage += "Ogiltigt personnummer\n";
		}
		if (editStreetAdressTF.getText() == null || editStreetAdressTF.getText().equals("")) {
			errorMessage += "Ogiltig gatuadress\n";
		}
		if (editZipCodeTF.getText() == null || editZipCodeTF.getText().equals("")) {
			errorMessage += "Ogiltigt postnummer\n";
		}
		if (editCityTF.getText() == null || editCityTF.getText().equals("")) {
			errorMessage += "Ogiltig postort\n";
		}
		if (editPhoneNumberTF.getText() == null || editPhoneNumberTF.getText().equals("")) {
			errorMessage += "Ogiltigt telefonnummer\n";
		}
		if (editEmailTF.getText() == null || editEmailTF.getText().equals("")) {
			errorMessage += "Ogiltig email\n";
		}
		if (editUsernameTF.getText() == null || editUsernameTF.getText().equals("")) {
			errorMessage += "Ogiltigt andv�ndarnamn\n";
		}
		if (editPasswordPF.getText() == null || editPasswordPF.getText().equals("")) {
			errorMessage += "Ogiltigt l�senord\n";
		}
		
		if (errorMessage.length() == 0) {
			return true;
		}else{
			Alert alert = new Alert(AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Ogiltiga f�lt");
            alert.setHeaderText(null);
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
		}
	}	
}
