package com.cartech.viewControllers;

import com.cartech.MainApplication;
import com.cartech.mainViewTabControllers.CarTabController;
import com.cartech.mainViewTabControllers.ServiceTabController;
import com.cartech.mainViewTabControllers.UserTabController;
import com.cartech.models.*;


import javafx.fxml.FXML;
import javafx.scene.control.*;

public class MainViewController {
	
	@FXML
	public TabPane mainViewTabPane;
	
	@FXML
	public Tab userTab, carTab, serviceTab;
	
	//Usertab  FXML variables used in UserTabController
	
	@FXML
	public TableView<UserModel> userTable;
	
	@FXML
	public TableView<CarModel> userCarsTable;
	
	@FXML
	public TableColumn<UserModel, String> userFirstNameColumn, userLastNameColumn, userPhoneNumberColumn;
	
	@FXML
	public TableColumn<CarModel, String> userCarBrandColumn, userCarModelColumn, userCarLicencePlateColumn;
	
	@FXML
	public Label userFirstNameLabel, userLastNameLabel, userSocialSecurityLabel, userStreetAdressLabel,userZipCodeLabel,
	userCityLabel, userEmailLabel, userPhoneNumberLabel, userUserNameLabel, userEmployeeLabel, userIdLabel;
	
	@FXML
	public TextField userSearchTextField;
	
	//CarTab FXML variables used in CarTabController
	
	@FXML
	public TableView<CarModel> carTable;
	
	@FXML
	public TableView<ServiceModel> carServicesTable;
	
	@FXML
	public TableColumn<CarModel, String> carBrandColumn, carModelColumn, carLicencePlateColumn;
	
	@FXML
	public TableColumn<ServiceModel, String> carServiceDateColumn, carServiceTimeColumn;
	
	@FXML
	public Label carIDLabel, carOwnerFirstAndLastNameLabel, carOwnerPhoneLabel,
	carBrandLabel, carModelLabel , carLicencePlateLabel;
	
	@FXML
	public TextField carSearchTextField;
	
	@FXML
	public TextArea carCommentsTextArea;
	
	//Servicetab FXML variables used in ServiceTabController
	
	@FXML
	public TableView<ServiceModel> serviceTable;
	
	@FXML
	public TableColumn<ServiceModel, String> serviceOwnerColumn, serviceCarColumn, serviceDateColumn, serviceTimeColumn;
	
	@FXML
	public Label serviceIDLabel, serviceDateLabel, serviceTimeLabel, serviceOwnerFirstAndLastNameLabel,
	serviceOwnerPhoneNumberLabel, serviceCarBrandLabel , serviceCarModelLabel, serviceCarLicenceplateLabel;
	
	@FXML
	public TextField serviceSearchTextField;
	
	@FXML
	public TextArea serviceCommentsTextArea;
	
	public MainApplication mainApplication;
	public UserTabController userTabController;
	public CarTabController carTabController;
	public ServiceTabController serviceTabController;
	
	public MainViewController(){
		userTabController = new UserTabController(this);
		carTabController = new CarTabController(this);
		serviceTabController = new ServiceTabController(this);
	}
	
	public void setMainApplication(MainApplication mainApplication){
		this.mainApplication = mainApplication;
	}
	
	//Runs when FXML file has initiated	
	@FXML
	private void initialize(){	 
		userTabController.runOnMainViewFXMLInit();
		carTabController.runOnMainViewFXMLInit();
		serviceTabController.runOnMainViewFXMLInit();
		
		updateAllTableViews();
		
	}
	
	public void updateAllTableViews(){
		handleUserSearchTextField();
		handleCarSearchTextField();
		handleServiceSearchTextField();
	}
	
	//Event methods must be in the same controller set in the FXML document to work	
	
	//User tab methods
	public void handleAddUserButton(){
		userTabController.handleAddUserButton();
	}
	
	public void handleEditUserButton(){
		userTabController.handleEditUserButton();
	}
	
	public void handleDeleteUserButton(){
		userTabController.handleDeleteUserButton();
	}
	
	public void handleUserSearchTextField(){
		userTabController.handleUserSearchTextField();
	}
	
	//Car tab methods
	public void handleAddCarButton(){
		carTabController.handleAddCarButton();
	}
	
	public void handleEditCarButton(){
		carTabController.handleEditCarButton();
	}
	
	public void handleDeleteCarButton(){
		carTabController.handleDeleteCarButton();
	}
	
	public void handleCarSearchTextField(){
		carTabController.handleCarSearchTextField();
	}
	
	//Service tab methods
	public void handleAddServiceButton(){
		serviceTabController.handleAddServiceButton();
	}
	
	public void handleEditServiceButton(){
		serviceTabController.handleEditServiceButton();
	}
	
	public void handleDeleteServiceButton(){
		serviceTabController.handleDeleteServiceButton();
	}
	
	public void handleServiceSearchTextField(){
		serviceTabController.handleServiceSearchTextField();
	}	
}
