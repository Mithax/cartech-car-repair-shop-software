package com.cartech.viewControllers;

import com.cartech.MainApplication;
import com.cartech.viewControllers.MainViewController;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;

public class RootController {
	
	@FXML
	private Menu addMenu, statisticsMenu;
	
	@FXML
	private MenuItem logoutMenuItem, changePasswordMenuItem;
	
    private MainViewController mainViewController;
    private MainApplication mainApplication;
    
    private int servicesOfCurrentAndPreviousYear;
    private int servicesOfCurrentAndPreviousMonth;
    private int averageServiceYearPerBrand;
    
    public RootController(){
    	servicesOfCurrentAndPreviousYear = 0;
    	servicesOfCurrentAndPreviousMonth = 1;
    	averageServiceYearPerBrand = 2;
    }
    
    
    public void setMainApplication(MainApplication mainApplication){
    	this.mainApplication = mainApplication;
    }
    
    public void setMainViewController(MainViewController mainViewController){
    	this.mainViewController = mainViewController;
    }

    @FXML
    private void handleAddUser() {
    	mainViewController.userTabController.handleAddUserButton();
    }

    @FXML
    private void handleAddCar() {
    	mainViewController.carTabController.handleAddCarButton();
    }

    @FXML
    private void handleAddService() {
    	mainViewController.serviceTabController.handleAddServiceButton();
    }
    
    @FXML
    private void handleServicesOfCurrentAndPreviousYear() {
    	mainApplication.showServiceStatisticsDialog(servicesOfCurrentAndPreviousYear);
    }
    
    @FXML
    private void handleServicesOfCurrentAndPreviousMonth() {
    	mainApplication.showServiceStatisticsDialog(servicesOfCurrentAndPreviousMonth);
    }

    @FXML
    private void handlesetAverageServiceYearPerBrand() {
    	mainApplication.showServiceStatisticsDialog(averageServiceYearPerBrand);
    }
 
    @FXML
    private void handleAboutTheApplication() {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Car Tech företagsapp");
        alert.setHeaderText(null);
        alert.setContentText("Skapad av Max Geissler, Teknikhögskolan -2016");

        alert.showAndWait();
    }
    
    @FXML
    private void handleChangePassword(){
    	mainApplication.showChangePassWordDialog();
    }
    
    @FXML
    private void handleLogout() {
        mainApplication.showLoginView();
    }

    @FXML
    private void handleExit() {
        System.exit(0);
    }
    
    public void showMenuItemsOnAdminLogin(){
    	changePasswordMenuItem.setVisible(true);
    	logoutMenuItem.setVisible(true);
    	addMenu.setVisible(true);
    	statisticsMenu.setVisible(true);
    
    }
    
    public void showMenuItemsOnCustomerLogin(){
    	changePasswordMenuItem.setVisible(true);
    	logoutMenuItem.setVisible(true);
    }      
   
}
