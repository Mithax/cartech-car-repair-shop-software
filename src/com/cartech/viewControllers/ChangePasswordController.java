package com.cartech.viewControllers;

import com.cartech.models.UserModel;
import com.cartech.repository.UserRepository;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

public class ChangePasswordController {
	
	@FXML
	private PasswordField currentPasswordPF, newPasswordPF, repeatNewPasswordPF;
	
	private Stage dialogStage;
	UserRepository userRepository;
	private UserModel currentUser;
	String errorMessage = "";
	
	public ChangePasswordController() {
		userRepository = new UserRepository();
	}
	
	public void setCurrentUser(UserModel currentUser){
		this.currentUser = currentUser;
	}
	
	public void setDialogStage(Stage dialogStage){
		this.dialogStage = dialogStage;
	}
	
	private void doesCurrentPasswordInputMatchUserPassword(){
		if (!currentPasswordPF.getText().equals(currentUser.getPassword())) {
			errorMessage += "Nuvarande l�senord matchar ej\n";
		}
	}
	
	private void doesNewPasswordFieldsMatch(){
		if (!newPasswordPF.getText().equals(repeatNewPasswordPF.getText())) {
			errorMessage += "F�lten f�r nytt l�senord matchar ej\n";
		}
	}
	
	private boolean isInputsValid(){
		
		if (currentPasswordPF.getText() == null || currentPasswordPF.getText().equals("")) {
			errorMessage += "Nuvarande l�senord ej ifyllt\n";
		}
		if (newPasswordPF.getText() == null || newPasswordPF.getText().equals("")) {
			errorMessage += "Nytt l�senord ej ifyllt\n";
		}
		if (repeatNewPasswordPF.getText() == null || repeatNewPasswordPF.getText().equals("")) {
			errorMessage += "Repetera nytt l�senord ej ifyllt\n";
		}
		if (errorMessage.length() > 0) {
			return false;
		}else{
			return true;
		}
	}	
	
	private void setNewPassword(){
		currentUser.setPassword(newPasswordPF.getText());
		userRepository.updateUserInDatabase(currentUser);
	}
	
	@FXML
	private void handleOk(){
		
		if (isInputsValid()) {			
			doesCurrentPasswordInputMatchUserPassword();
			doesNewPasswordFieldsMatch();

			if (errorMessage.length() == 0) {
				setNewPassword();
	            dialogStage.close();
			}
			else {
				Alert alert = new Alert(AlertType.ERROR);
	            alert.initOwner(dialogStage);
	            alert.setTitle("Gick ej att byta l�senord");
	            alert.setHeaderText(null);
	            alert.setContentText(errorMessage);
	            errorMessage = "";
	
	            alert.showAndWait();
			}
		}else {
			Alert alert = new Alert(AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Alla f�lt m�ste fyllas i");
            alert.setHeaderText(null);
            alert.setContentText(errorMessage);
            errorMessage = "";

            alert.showAndWait();
		}
	}
	
	@FXML
	private void handleCancel(){
		dialogStage.close();
	}

}
