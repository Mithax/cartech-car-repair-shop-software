package com.cartech.repository;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.sql.rowset.JdbcRowSet;
import javax.sql.rowset.RowSetProvider;

import com.cartech.models.CarModel;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class CarRepository {
	
	private static final String datePattern = "dd.MM.yyyy";
    private static final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(datePattern);
	
	private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	private static final String DATABASE_URL = "jdbc:mysql://localhost:3306/cartech";
	private static final String DATABASE_USER = "root";
	private static final String DATABASE_PASSWORD = "password";
	private JdbcRowSet jdbcrowset;
	
	public CarRepository() {
		
		try {
			Class.forName(JDBC_DRIVER);
			jdbcrowset = RowSetProvider.newFactory().createJdbcRowSet();
			jdbcrowset.setUrl(DATABASE_URL);
			jdbcrowset.setUsername(DATABASE_USER);
			jdbcrowset.setPassword(DATABASE_PASSWORD);
			jdbcrowset.setCommand("SELECT * FROM cars");
			jdbcrowset.execute();
			
		} catch (Exception exception) {
			System.out.println("Exception in CarRepository Constructor: " + exception.getMessage());
		}
	}
	
	private String formatLocalDateToString(LocalDate localDate){
		return dateFormatter.format(localDate);
	}
	
	private LocalDate formatStringToLocalDate(String localDateString){
		return dateFormatter.parse(localDateString, LocalDate::from);
	}
	
	public void addCarToDatabase(CarModel carModel){
		try {
			jdbcrowset.moveToInsertRow();
			
			jdbcrowset.updateLong("userID", carModel.getOwnerID());
			jdbcrowset.updateString("created", formatLocalDateToString(carModel.getCarAddedToApplication()));
			jdbcrowset.updateString("brand", carModel.getBrand());
			jdbcrowset.updateString("model", carModel.getModel());
			jdbcrowset.updateString("licencePlate", carModel.getLicencePlate());
			jdbcrowset.updateString("comments", carModel.getComments());
			
			jdbcrowset.insertRow();
			jdbcrowset.moveToCurrentRow();
			
		} catch (SQLException sqlException) {
			System.out.println("SQL Exception in CarRepository.addCarToDatabase: " + sqlException.getMessage());
			try {
				jdbcrowset.rollback();
				carModel = null;
			} catch (Exception exception) {
				System.out.println("Exception in CarRepository.addCarToDatabase.rollback: " + exception.getMessage());
			}
		}catch (Exception exception) {
			System.out.println("Exception in CarRepository.addCarToDatabase: " + exception.getMessage());
		}
	}
	
	public void updateCarInDatabase(CarModel carModel){
		try {
			jdbcrowset.first();
			do{
				if (jdbcrowset.getLong("carID") == carModel.getID()) {
					jdbcrowset.updateLong("userID", carModel.getOwnerID());
					jdbcrowset.updateString("brand", carModel.getBrand());
					jdbcrowset.updateString("model", carModel.getModel());
					jdbcrowset.updateString("licencePlate", carModel.getLicencePlate());
					jdbcrowset.updateString("comments", carModel.getComments());
					
					jdbcrowset.updateRow();
					
				}
			}while(jdbcrowset.next() != false);
			
		} catch (SQLException sqlException) {
			System.out.println("SQL Exception in CarRepository.updateCarInDatabase: " + sqlException.getMessage());
			try {
				jdbcrowset.rollback();
				carModel = null;
			} catch (Exception exception) {
				System.out.println("Exception in CarRepository.updateCarInDatabase.rollback: " + exception.getMessage());
			}
		}catch (Exception exception) {
			System.out.println("Exception in CarRepository.updateCarInDatabase: " + exception.getMessage());
		}
	}
	
	public void deleteCarInDatabase(CarModel carModel){
		try {
			jdbcrowset.first();
			do{
				if (jdbcrowset.getLong("carID") == carModel.getID()) {
					jdbcrowset.deleteRow();
				}
			}while(jdbcrowset.next() != false);
			
		} catch (SQLException sqlException) {
			System.out.println("SQL Error in CarRepository.deleteCarInDatabase: " + sqlException.getMessage());
			try {
				jdbcrowset.rollback();
				carModel = null;
			} catch (Exception exception) {
				System.out.println("Exception in CarRepository.deleteCarInDatabase.rollback: " + exception.getMessage());
			}
		}catch (Exception exception) {
			System.out.println("Exception in CarRepository.deleteCarInDatabase: " + exception.getMessage());
		}
	}
	
	public ObservableList<CarModel> getCarListFromDatabaseForTableView(String search) {
		ObservableList<CarModel> carList = FXCollections.observableArrayList();
		carList.clear();
		try {
			jdbcrowset.first();
			do{
			CarModel carModel = new CarModel();
			
			carModel.setID(jdbcrowset.getLong("carID"));
			carModel.setCarAddedToApplication(formatStringToLocalDate(jdbcrowset.getString("created")));
			carModel.setOwnerID(jdbcrowset.getLong("userID"));
			carModel.setBrand(jdbcrowset.getString("brand"));
			carModel.setModel(jdbcrowset.getString("model"));
			carModel.setLicencePlate(jdbcrowset.getString("licencePlate"));
			carModel.setComments(jdbcrowset.getString("comments"));
		
			if(search.equals("") || search.isEmpty()){
				carList.add(carModel);
			}else if(carModel.toString().toLowerCase().contains(search.toLowerCase())){
				carList.add(carModel);
			}
						
			}while(jdbcrowset.next() != false);
		} catch (SQLException sqlException) {
			System.out.println("SQL Exception in CarRepository.getCarListFromDatabaseForTableView: " + sqlException.getMessage());
			try {
				jdbcrowset.rollback();
				search = null;
			} catch (Exception exception) {
				System.out.println("Exception in CarRepository.getCarListFromDatabaseForTableView.rollback: " + exception.getMessage());
			}
		}catch (Exception exception) {
			System.out.println("Exception in CarRepository.getCarListFromDatabaseForTableView: " + exception.getMessage());
		}
		
        return carList;
    }
	
	public CarModel getCarFromDataBaseWithUserID(Long ID){
		CarModel carModel = new CarModel();
		
		try {
			jdbcrowset.first();
			do{
				if(ID == jdbcrowset.getLong("userID")){
				
					carModel.setID(jdbcrowset.getLong("carID"));
					carModel.setCarAddedToApplication(formatStringToLocalDate(jdbcrowset.getString("created")));
					carModel.setOwnerID(jdbcrowset.getLong("userID"));
					carModel.setBrand(jdbcrowset.getString("brand"));
					carModel.setModel(jdbcrowset.getString("model"));
					carModel.setLicencePlate(jdbcrowset.getString("licencePlate"));
					carModel.setComments(jdbcrowset.getString("comments"));
				}
						
			}while(jdbcrowset.next() != false);
		} catch (SQLException sqlException) {
			System.out.println("SQL Exception in CarRepository.getCarFromDataBaseWithUserID: " + sqlException.getMessage());
			try {
				jdbcrowset.rollback();
				ID = null;
			} catch (Exception exception) {
				System.out.println("Exception in CarRepository.getCarFromDataBaseWithUserID.rollback: " + exception.getMessage());
			}
		}catch (Exception exception) {
			System.out.println("Exception in CarRepository.getCarFromDataBaseWithUserID: " + exception.getMessage());
		}
	
		return carModel;
	}
	
	public ObservableList<CarModel> getCarListFromDataBaseForTableViewWithUserID(Long ID) {
		ObservableList<CarModel> carList = FXCollections.observableArrayList();
		carList.clear();
		try {
			jdbcrowset.first();
			do{
			CarModel carModel = new CarModel();
			
			carModel.setID(jdbcrowset.getLong("carID"));
			carModel.setCarAddedToApplication(formatStringToLocalDate(jdbcrowset.getString("created")));
			carModel.setOwnerID(jdbcrowset.getLong("userID"));
			carModel.setBrand(jdbcrowset.getString("brand"));
			carModel.setModel(jdbcrowset.getString("model"));
			carModel.setLicencePlate(jdbcrowset.getString("licencePlate"));	
			carModel.setComments(jdbcrowset.getString("comments"));
			
			if(ID == jdbcrowset.getLong("userID")){
				carList.add(carModel);
			}
						
			}while(jdbcrowset.next() != false);
			
		} catch (SQLException sqlException) {
			System.out.println("SQL Exception in CarRepository.getCarListFromDataBaseForTableViewWithUserID: " + sqlException.getMessage());
			try {
				jdbcrowset.rollback();
				ID = null;
			} catch (Exception exception) { 
				System.out.println("Exception in CarRepository.getCarListFromDataBaseForTableViewWithUserID.rollback: " + exception.getMessage());
			}
		}catch (Exception exception) {
			System.out.println("Exception in CarRepository.getCarListFromDataBaseForTableViewWithUserID: " + exception.getMessage());
		}
        return carList;
    }
	
	public CarModel getCarFromDataBaseWithCarID(Long ID){
		CarModel carModel = new CarModel();
		
		try {
			jdbcrowset.first();
			do{
				if(ID == jdbcrowset.getLong("carID")){
				
					carModel.setID(jdbcrowset.getLong("carID"));
					carModel.setCarAddedToApplication(formatStringToLocalDate(jdbcrowset.getString("created")));
					carModel.setOwnerID(jdbcrowset.getLong("userID"));
					carModel.setBrand(jdbcrowset.getString("brand"));
					carModel.setModel(jdbcrowset.getString("model"));
					carModel.setLicencePlate(jdbcrowset.getString("licencePlate"));
					carModel.setComments(jdbcrowset.getString("comments"));
				}
						
			}while(jdbcrowset.next() != false);
		} catch (SQLException sqlException) {
			System.out.println("SQL Exception in CarRepository.getCarFromDataBaseWithUserID: " + sqlException.getMessage());
			try {
				jdbcrowset.rollback();
				ID = null;
			} catch (Exception exception) {
				System.out.println("Exception in CarRepository.getCarFromDataBaseWithUserID.rollback: " + exception.getMessage());
			}
		}catch (Exception exception) {
			System.out.println("Exception in CarRepository.getCarFromDataBaseWithUserID: " + exception.getMessage());
		}
	
		return carModel;
	}
	
	public ObservableList<CarModel> getCarListFromDataBaseForTableViewWithCarID(Long ID) {
		ObservableList<CarModel> carList = FXCollections.observableArrayList();
		carList.clear();
		try {
			jdbcrowset.first();
			do{
			CarModel carModel = new CarModel();
			
			carModel.setID(jdbcrowset.getLong("carID"));
			carModel.setCarAddedToApplication(formatStringToLocalDate(jdbcrowset.getString("created")));
			carModel.setOwnerID(jdbcrowset.getLong("userID"));
			carModel.setBrand(jdbcrowset.getString("brand"));
			carModel.setModel(jdbcrowset.getString("model"));
			carModel.setLicencePlate(jdbcrowset.getString("licencePlate"));	
			carModel.setComments(jdbcrowset.getString("comments"));
			
			if(ID == jdbcrowset.getLong("carID")){
				carList.add(carModel);
			}
						
			}while(jdbcrowset.next() != false);
			
		} catch (SQLException sqlException) {
			System.out.println("SQL Exception in CarRepository.getCarListFromDataBaseForTableViewWithUserID: " + sqlException.getMessage());
			try {
				jdbcrowset.rollback();
				ID = null;
			} catch (Exception exception) { 
				System.out.println("Exception in CarRepository.getCarListFromDataBaseForTableViewWithUserID.rollback: " + exception.getMessage());
			}
		}catch (Exception exception) {
			System.out.println("Exception in CarRepository.getCarListFromDataBaseForTableViewWithUserID: " + exception.getMessage());
		}
        return carList;
    }	
}
