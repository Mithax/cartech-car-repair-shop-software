package com.cartech.repository;

import java.sql.SQLException;

import javax.sql.rowset.JdbcRowSet;
import javax.sql.rowset.RowSetProvider;

import com.cartech.models.UserModel;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


public class UserRepository {
	
	private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	private static final String DATABASE_URL = "jdbc:mysql://localhost:3306/cartech";
	private static final String DATABASE_USER = "root";
	private static final String DATABASE_PASSWORD = "password";
	private JdbcRowSet jdbcrowset = null;
	
	public UserRepository() {
		
		try {
			Class.forName(JDBC_DRIVER);
			jdbcrowset = RowSetProvider.newFactory().createJdbcRowSet();
			jdbcrowset.setUrl(DATABASE_URL);
			jdbcrowset.setUsername(DATABASE_USER);
			jdbcrowset.setPassword(DATABASE_PASSWORD);
			jdbcrowset.setCommand("SELECT * FROM users");
			jdbcrowset.execute();
			
		} catch (Exception exception) {
			System.out.println("Exception in UserRepository.Constructor: " + exception.getMessage());
		}
	}
	
	public void addUserToDatabase(UserModel userModel){
		try {
			jdbcrowset.moveToInsertRow();	
			
			jdbcrowset.updateString("firstName", userModel.getFirstName());
			jdbcrowset.updateString("lastName", userModel.getLastName());
			jdbcrowset.updateString("socialSecurityNumber", userModel.getSocialSecurityNumber());
			jdbcrowset.updateString("streetAdress", userModel.getStreetAdress());
			jdbcrowset.updateString("zipCode", userModel.getZipCode());
			jdbcrowset.updateString("city", userModel.getCity());
			jdbcrowset.updateString("phoneNumber", userModel.getPhoneNumber());
			jdbcrowset.updateString("email", userModel.getEmail());
			jdbcrowset.updateString("username", userModel.getUsername());
			jdbcrowset.updateString("password", userModel.getPassword());
			jdbcrowset.updateBoolean("employee", userModel.isEmployee());
			
			jdbcrowset.insertRow();
			jdbcrowset.moveToCurrentRow();
			
		} catch (SQLException sqlException) {
			System.out.println("SQL Exception in UserRepository.addUserToDatabase: " + sqlException.getMessage());
			try {
				jdbcrowset.rollback();
				userModel = null;
			} catch (Exception exception) {
				System.out.println("Exception in UserRepository.addUserToDatabase.rollback: " + exception.getMessage());
			} 			
		}catch (Exception exception) {
				System.out.println("Exception in UserRepository.addUserToDatabase: " + exception.getMessage());
		}
	}
	
	public void updateUserInDatabase(UserModel userModel){
		try {
			jdbcrowset.first();
			do{
				if (jdbcrowset.getLong("userID") == userModel.getID()) {

					jdbcrowset.updateString("firstName", userModel.getFirstName());
					jdbcrowset.updateString("lastName", userModel.getLastName());
					jdbcrowset.updateString("socialSecurityNumber", userModel.getSocialSecurityNumber());
					jdbcrowset.updateString("streetAdress", userModel.getStreetAdress());
					jdbcrowset.updateString("zipCode", userModel.getZipCode());
					jdbcrowset.updateString("city", userModel.getCity());
					jdbcrowset.updateString("phoneNumber", userModel.getPhoneNumber());
					jdbcrowset.updateString("email", userModel.getEmail());
					jdbcrowset.updateString("username", userModel.getUsername());
					jdbcrowset.updateString("password", userModel.getPassword());
					jdbcrowset.updateBoolean("employee", userModel.isEmployee());
					
					jdbcrowset.updateRow();
					
				}
			}while(jdbcrowset.next() != false);
		} catch (SQLException sqlException) {
			System.out.println("SQL Exception in UserRepository.updateUserInDatabase: " + sqlException.getMessage());
			try {
				jdbcrowset.rollback();
				userModel = null;
			} catch (Exception exception) {
				System.out.println("Exception in UserRepository.updateUserInDatabase.rollback: " + exception.getMessage());
			} 			
		}catch (Exception exception) {
				System.out.println("Exception in UserRepository.updateUserInDatabase: " + exception.getMessage());
		}
	}
	
	public void deleteUserInDatabase(UserModel userModel){
		try {
			jdbcrowset.first();
			do{
				if (jdbcrowset.getLong("userID") == userModel.getID()) {
					jdbcrowset.deleteRow();
				}
			}while(jdbcrowset.next() != false);
		} catch (SQLException sqlException) {
			System.out.println("SQL Exception in UserRepository.deleteUserInDatabase: " + sqlException.getMessage());
			try {
				jdbcrowset.rollback();
				userModel = null;
			} catch (Exception exception) {
				System.out.println("Exception in UserRepository.deleteUserInDatabase.rollback: " + exception.getMessage());
			} 			
		}catch (Exception exception) {
				System.out.println("Exception in UserRepository.deleteUserInDatabase: " + exception.getMessage());
		}
	}
	
	public ObservableList<UserModel> getUserListFromDatabaseForTableView(String search) {
		ObservableList<UserModel> userList = FXCollections.observableArrayList();
		userList.clear();
		try {
			jdbcrowset.first();
			do{
			UserModel userModel = new UserModel();
			
			userModel.setID(jdbcrowset.getLong("userID"));
			userModel.setFirstName(jdbcrowset.getString("firstName"));
			userModel.setLastName(jdbcrowset.getString("lastName"));
			userModel.setSocialSecurityNumber(jdbcrowset.getString("socialSecurityNumber"));
			userModel.setStreetAdress(jdbcrowset.getString("streetAdress"));
			userModel.setZipCode(jdbcrowset.getString("zipCode"));
			userModel.setCity(jdbcrowset.getString("city"));
			userModel.setPhoneNumber(jdbcrowset.getString("phoneNumber"));
			userModel.setEmail(jdbcrowset.getString("email"));
			userModel.setUsername(jdbcrowset.getString("username"));
			userModel.setPassword(jdbcrowset.getString("password"));
			userModel.setEmployee(jdbcrowset.getBoolean("employee"));
			
			if(search.equals("") || search.isEmpty()){
				userList.add(userModel);
			}else if(userModel.toString().toLowerCase().contains(search.toLowerCase())){
				userList.add(userModel);
			}
						
			}while(jdbcrowset.next() != false);
		} catch (SQLException sqlException) {
			System.out.println("SQL Exception in UserRepository.getUserListFromDatabaseForTableView: " + sqlException.getMessage());
			try {
				jdbcrowset.rollback();
				search = null;
			} catch (Exception exception) {
				System.out.println("Exception in UserRepository.getUserListFromDatabaseForTableView.rollback: " + exception.getMessage());
			} 			
		}catch (Exception exception) {
				System.out.println("Exception in UserRepository.getUserListFromDatabaseForTableView: " + exception.getMessage());
		}
        return userList;
    }
	
	public UserModel getUserFromDataBaseWithUserID(Long ID){
		UserModel userModel = new UserModel();
		try {
			jdbcrowset.first();
			do{
				if(ID == jdbcrowset.getLong("userID")){
					userModel.setID(jdbcrowset.getLong("userID"));
					userModel.setFirstName(jdbcrowset.getString("firstName"));
					userModel.setLastName(jdbcrowset.getString("lastName"));
					userModel.setSocialSecurityNumber(jdbcrowset.getString("socialSecurityNumber"));
					userModel.setStreetAdress(jdbcrowset.getString("streetAdress"));
					userModel.setZipCode(jdbcrowset.getString("zipCode"));
					userModel.setCity(jdbcrowset.getString("city"));
					userModel.setPhoneNumber(jdbcrowset.getString("phoneNumber"));
					userModel.setEmail(jdbcrowset.getString("email"));
					userModel.setUsername(jdbcrowset.getString("username"));
					userModel.setPassword(jdbcrowset.getString("password"));
					userModel.setEmployee(jdbcrowset.getBoolean("employee"));		
				}
						
			}while(jdbcrowset.next() != false);
		} catch (SQLException sqlException) {
			System.out.println("SQL Exception in UserRepository.getUserFromDataBaseWithUserID: " + sqlException.getMessage());
			try {
				jdbcrowset.rollback();
				ID = null;
			} catch (Exception exception) {
				System.out.println("Exception in UserRepository.getUserFromDataBaseWithUserID.rollback: " + exception.getMessage());
			} 			
		}catch (Exception exception) {
				System.out.println("Exception in UserRepository.getUserFromDataBaseWithUserID: " + exception.getMessage());
		}
	
		return userModel;
	}
	
	public UserModel getUserFromDataBaseWithUserName(String userName){
		UserModel userModel = new UserModel();
		try {
			jdbcrowset.first();
			do{
				if(userName.equalsIgnoreCase(jdbcrowset.getString("username"))){
					userModel.setID(jdbcrowset.getLong("userID"));
					userModel.setFirstName(jdbcrowset.getString("firstName"));
					userModel.setLastName(jdbcrowset.getString("lastName"));
					userModel.setSocialSecurityNumber(jdbcrowset.getString("socialSecurityNumber"));
					userModel.setStreetAdress(jdbcrowset.getString("streetAdress"));
					userModel.setZipCode(jdbcrowset.getString("zipCode"));
					userModel.setCity(jdbcrowset.getString("city"));
					userModel.setPhoneNumber(jdbcrowset.getString("phoneNumber"));
					userModel.setEmail(jdbcrowset.getString("email"));
					userModel.setUsername(jdbcrowset.getString("username"));
					userModel.setPassword(jdbcrowset.getString("password"));
					userModel.setEmployee(jdbcrowset.getBoolean("employee"));		
				}
						
			}while(jdbcrowset.next() != false);
		} catch (SQLException sqlException) {
			System.out.println("SQL Exception in UserRepository.getUserFromDataBaseWithUserName: " + sqlException.getMessage());
			try {
				jdbcrowset.rollback();
				userName = null;
			} catch (Exception exception) {
				System.out.println("Exception in UserRepository.getUserFromDataBaseWithUserName.rollback: " + exception.getMessage());
			} 			
		}catch (Exception exception) {
				System.out.println("Exception in UserRepository.getUserFromDataBaseWithUserName: " + exception.getMessage());
		}
	
		return userModel;
	}
	
	public ObservableList<UserModel> getUserListFromDataBaseForTableViewWithID(Long ID) {
		ObservableList<UserModel> userList = FXCollections.observableArrayList();
		userList.clear();
		try {
			jdbcrowset.first();
			do{
			UserModel userModel = new UserModel();
			
			userModel.setID(jdbcrowset.getLong("userID"));
			userModel.setFirstName(jdbcrowset.getString("firstName"));
			userModel.setLastName(jdbcrowset.getString("lastName"));
			userModel.setSocialSecurityNumber(jdbcrowset.getString("socialSecurityNumber"));
			userModel.setStreetAdress(jdbcrowset.getString("streetAdress"));
			userModel.setZipCode(jdbcrowset.getString("zipCode"));
			userModel.setCity(jdbcrowset.getString("city"));
			userModel.setPhoneNumber(jdbcrowset.getString("phoneNumber"));
			userModel.setEmail(jdbcrowset.getString("email"));
			userModel.setUsername(jdbcrowset.getString("username"));
			userModel.setPassword(jdbcrowset.getString("password"));
			userModel.setEmployee(jdbcrowset.getBoolean("employee"));
			
			if(ID == jdbcrowset.getLong("userID")){
				userList.add(userModel);
			}
						
			}while(jdbcrowset.next() != false);
		} catch (SQLException sqlException) {
			System.out.println("SQL Exception in UserRepository.getUserListFromDataBaseForTableViewWithID: " + sqlException.getMessage());
			try {
				jdbcrowset.rollback();
				ID = null;
			} catch (Exception exception) {
				System.out.println("Exception in UserRepository.getUserListFromDataBaseForTableViewWithID.rollback: " + exception.getMessage());
			} 			
		}catch (Exception exception) {
				System.out.println("Exception in UserRepository.getUserListFromDataBaseForTableViewWithID: " + exception.getMessage());
		}
        return userList;
    }	
}
