package com.cartech.repository;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.sql.rowset.JdbcRowSet;
import javax.sql.rowset.RowSetProvider;

import com.cartech.models.ServiceModel;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class ServiceRepository {
	
	private static final String datePattern = "dd.MM.yyyy";
    private static final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(datePattern);
	
	private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	private static final String DATABASE_URL = "jdbc:mysql://localhost:3306/cartech";
	private static final String DATABASE_USER = "root";
	private static final String DATABASE_PASSWORD = "password";
	private JdbcRowSet jdbcrowset;
	
	public ServiceRepository() {
		
		try {
			Class.forName(JDBC_DRIVER);
			jdbcrowset = RowSetProvider.newFactory().createJdbcRowSet();
			jdbcrowset.setUrl(DATABASE_URL);
			jdbcrowset.setUsername(DATABASE_USER);
			jdbcrowset.setPassword(DATABASE_PASSWORD);
			jdbcrowset.setCommand("SELECT * FROM services");
			jdbcrowset.execute();
			
		} catch (Exception exception) {
			System.out.println("Exception in ServiceRepository Constructor: " + exception.getMessage());
		}
	}
	
	private String formatLocalDateToString(LocalDate localDate){
		return dateFormatter.format(localDate);
	}
	
	private LocalDate formatStringToLocalDate(String localDateString){
		return dateFormatter.parse(localDateString, LocalDate::from);
	}
	
	public void addServiceToDatabase(ServiceModel serviceModel){
		try {
			jdbcrowset.moveToInsertRow();

			jdbcrowset.updateLong("carID", serviceModel.getCarToBeServedID());
			jdbcrowset.updateString("date", formatLocalDateToString(serviceModel.getServiceDate()));
			jdbcrowset.updateString("timeHour", serviceModel.getServiceTimeHour());
			jdbcrowset.updateString("timeMinute", serviceModel.getServiceTimeMinute());
			jdbcrowset.updateString("comments", serviceModel.getComments());
			jdbcrowset.updateString("serviceOwner", serviceModel.getServiceOwnerProperty().get());
			jdbcrowset.updateString("serviceCar", serviceModel.getServiceCarProperty().get());
			
			jdbcrowset.insertRow();
			jdbcrowset.moveToCurrentRow();
			
		} catch (SQLException sqlException) {
			System.out.println("SQL Exception in ServiceRepository.addServiceToDatabase: " + sqlException.getMessage());
			try {
				jdbcrowset.rollback();
				serviceModel = null;
			} catch (Exception exception) {
				System.out.println("Exception in ServiceRepository.addServiceToDatabase.rollback: " + exception.getMessage());
			}
		}catch (Exception exception) {
			System.out.println("Exception in ServiceRepository.addServiceToDatabase: " + exception.getMessage());
		}
	}
	
	public void updateServiceInDatabase(ServiceModel serviceModel){
		try {
			jdbcrowset.first();
			do{
				if (jdbcrowset.getLong("serviceID") == serviceModel.getID()) {
					
					jdbcrowset.updateLong("carID", serviceModel.getCarToBeServedID());
					jdbcrowset.updateString("date", formatLocalDateToString(serviceModel.getServiceDate()));
					jdbcrowset.updateString("timeHour", serviceModel.getServiceTimeHour());
					jdbcrowset.updateString("timeMinute", serviceModel.getServiceTimeMinute());
					jdbcrowset.updateString("comments", serviceModel.getComments());
					jdbcrowset.updateString("serviceOwner", serviceModel.getServiceOwnerProperty().get());
					jdbcrowset.updateString("serviceCar", serviceModel.getServiceCarProperty().get());
					
					jdbcrowset.updateRow();				
				}
			}while(jdbcrowset.next() != false);
			
		} catch (SQLException sqlException) {
			System.out.println("SQL Exception in ServiceRepository.updateServiceInDatabase: " + sqlException.getMessage());
			try {
				jdbcrowset.rollback();
				serviceModel = null;
			} catch (Exception exception) {
				System.out.println("Exception in ServiceRepository.updateServiceInDatabase.rollback: " + exception.getMessage());
			}
		}catch (Exception exception) {
			System.out.println("Exception in ServiceRepository.updateServiceInDatabase: " + exception.getMessage());
		}
	}
	
	public void deleteServiceInDatabase(ServiceModel serviceModel){
		try {
			jdbcrowset.first();
			do{
				if (jdbcrowset.getLong("serviceID") == serviceModel.getID()) {
					jdbcrowset.deleteRow();
				}
			}while(jdbcrowset.next() != false);
			
		} catch (SQLException sqlException) {
			System.out.println("SQL Error in ServiceRepository.deleteServiceInDatabase: " + sqlException.getMessage());
			try {
				jdbcrowset.rollback();
				serviceModel = null;
			} catch (Exception exception) {
				System.out.println("Exception in ServiceRepository.deleteServiceInDatabase.rollback: " + exception.getMessage());
			}
		}catch (Exception exception) {
			System.out.println("Exception in ServiceRepository.deleteServiceInDatabase: " + exception.getMessage());
		}
	}
	
	public ObservableList<ServiceModel> getServiceListFromDatabaseForTableView(String search) {
		ObservableList<ServiceModel> serviceList = FXCollections.observableArrayList();
		serviceList.clear();
		try {
			jdbcrowset.first();
			do{
			ServiceModel serviceModel = new ServiceModel();
			
			serviceModel.setID(jdbcrowset.getLong("serviceID"));
			serviceModel.setCarToBeServedID(jdbcrowset.getLong("carID"));
			serviceModel.setServiceDate(formatStringToLocalDate(jdbcrowset.getString("date")));
			serviceModel.setServiceTimeHour(jdbcrowset.getString("timeHour"));
			serviceModel.setServiceTimeMinute(jdbcrowset.getString("timeMinute"));
			serviceModel.setComments(jdbcrowset.getString("comments"));
			serviceModel.setServiceOwnerProperty(jdbcrowset.getString("serviceOwner"));
			serviceModel.setServiceCarProperty(jdbcrowset.getString("serviceCar"));
		
			if(search.equals("") || search.isEmpty()){
				serviceList.add(serviceModel);
			}else if(serviceModel.toString().toLowerCase().contains(search.toLowerCase())){
				serviceList.add(serviceModel);
			}
						
			}while(jdbcrowset.next() != false);
		} catch (SQLException sqlException) {
			System.out.println("SQL Exception in ServiceRepository.getServiceListFromDatabaseForTableView: " + sqlException.getMessage());
			try {
				jdbcrowset.rollback();
				search = null;
			} catch (Exception exception) {
				System.out.println("Exception in ServiceRepository.getServiceListFromDatabaseForTableView.rollback: " + exception.getMessage());
			}
		}catch (Exception exception) {
			System.out.println("Exception in ServiceRepository.getServiceListFromDatabaseForTableView: " + exception.getMessage());
		}
		
        return serviceList;
    }
	
	public ObservableList<ServiceModel> getServiceListFromDataBaseForTableViewWithCarID(Long ID) {
		ObservableList<ServiceModel> serviceList = FXCollections.observableArrayList();
		serviceList.clear();
		try {
			jdbcrowset.first();
			do{
			ServiceModel serviceModel = new ServiceModel();
			
			serviceModel.setID(jdbcrowset.getLong("serviceID"));
			serviceModel.setCarToBeServedID(jdbcrowset.getLong("carID"));
			serviceModel.setServiceDate(formatStringToLocalDate(jdbcrowset.getString("date")));
			serviceModel.setServiceTimeHour(jdbcrowset.getString("timeHour"));
			serviceModel.setServiceTimeMinute(jdbcrowset.getString("timeMinute"));
			serviceModel.setComments(jdbcrowset.getString("comments"));
			serviceModel.setServiceOwnerProperty(jdbcrowset.getString("serviceOwner"));
			serviceModel.setServiceCarProperty(jdbcrowset.getString("serviceCar"));
			
			if(ID == jdbcrowset.getLong("carID")){
				serviceList.add(serviceModel);
			}
						
			}while(jdbcrowset.next() != false);
			
		} catch (SQLException sqlException) {
			System.out.println("SQL Exception in ServiceRepository.getServiceListFromDataBaseForTableViewWithCarID: " + sqlException.getMessage());
			try {
				jdbcrowset.rollback();
				ID = null;
			} catch (Exception exception) { 
				System.out.println("Exception in ServiceRepository.getServiceListFromDataBaseForTableViewWithCarID.rollback: " + exception.getMessage());
			}
		}catch (Exception exception) {
			System.out.println("Exception in ServiceRepository.getServiceListFromDataBaseForTableViewWithCarID: " + exception.getMessage());
		}
        return serviceList;
    }
}
